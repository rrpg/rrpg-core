CSC = mcs
BUILD_DIR = build
SRC_DIR = src

LIBRARY = $(BUILD_DIR)/rrpg-core.dll
CSC_FLAGS = -out:$(LIBRARY) -target:library

SOURCES =

build: $(SOURCES)
	$(CSC) $(SOURCES) $(CSC_FLAGS)

all: build
